package com.example.loginandpassword.fragments.creditCard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.UseCase.GetCreditCardUseCase
import com.example.domain.model.CreditCardDto
import kotlinx.coroutines.launch

class CreditCardViewModel(private val creditCardUseCase: GetCreditCardUseCase) : ViewModel() {

    private val _creditCardLiveData = MutableLiveData<List<CreditCardDto>>()
    val creditCardLiveData: LiveData<List<CreditCardDto>> = _creditCardLiveData

    fun getCreditCard(count: Int) {
        viewModelScope.launch {
            _creditCardLiveData.value = creditCardUseCase.execute(count)
        }
    }
}