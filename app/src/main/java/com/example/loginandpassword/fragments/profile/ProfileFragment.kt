package com.example.loginandpassword.fragments.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.domain.model.ProfileDivider
import com.example.domain.model.ProfileItem
import com.example.loginandpassword.databinding.FragmentProfileBinding
import com.example.loginandpassword.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding>()
{

    private val viewModel by viewModel<ProfileViewModel>()

    private val adapter = ProfileAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater ,
        container: ViewGroup?
    ): FragmentProfileBinding = FragmentProfileBinding.inflate(inflater ,container ,false)

    override fun FragmentProfileBinding.onBindView(saveInstanceState: Bundle?)
    {
        back.setOnClickListener {
            navController.popBackStack()
        }
        items.adapter = adapter
        viewModel.getUserInfo()
        viewModel.userInfoLiveData.observe(viewLifecycleOwner) { userProfile ->

            adapter.submitList(
                listOf(
                    ProfileDivider ,
                    ProfileItem("number" ,userProfile.number) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileNumberFragment())
                    } ,
                    ProfileDivider ,
                    ProfileItem("name" ,userProfile.name) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileNameFragment())
                    } ,
                    ProfileDivider ,
                    ProfileItem("age" ,userProfile.age.toString()) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileAgeFragment())
                    } ,
                    ProfileDivider ,
                    ProfileItem("email" ,userProfile.email) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileEmailFragment())
                    } ,
                    ProfileDivider ,
                )
            )
        }
    }
}