package com.example.loginandpassword.fragments.profile.number

import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.EditNumberUseCase

class ProfileNumberViewModel(private val editNumberUseCase: EditNumberUseCase) : ViewModel()
{
    fun editNumber(number: String)
    {
        editNumberUseCase.execute(number)
    }
}