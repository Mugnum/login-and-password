package com.example.loginandpassword.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.loginandpassword.fragments.base.BaseFragment
import com.example.loginandpassword.databinding.FragmentMainBinding

class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun createViewBinding(
        inflater: LayoutInflater ,
        container: ViewGroup?
    ): FragmentMainBinding = FragmentMainBinding.inflate(inflater ,container ,false)

    override fun FragmentMainBinding.onBindView(saveInstanceState: Bundle?) {
        name.text = "Hello"
        name.setOnClickListener {
            navController.navigate(MainFragmentDirections.navigateToProfileFragment())
        }

    }
}