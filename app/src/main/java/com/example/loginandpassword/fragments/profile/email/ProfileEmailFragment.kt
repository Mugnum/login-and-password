package com.example.loginandpassword.fragments.profile.email

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.loginandpassword.databinding.FragmentProfileEmailBinding
import com.example.loginandpassword.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileEmailFragment : BaseFragment<FragmentProfileEmailBinding>()
{

    private val viewModel by viewModel<ProfileEmailViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater ,
        container: ViewGroup?
    ): FragmentProfileEmailBinding = FragmentProfileEmailBinding.inflate(inflater ,container ,false)

    override fun FragmentProfileEmailBinding.onBindView(saveInstanceState: Bundle?)
    {
        submit.setOnClickListener {
            viewModel.editEmail(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}