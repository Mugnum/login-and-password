package com.example.loginandpassword.fragments.profile.age

import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.EditAgeUseCase

class ProfileAgeViewModel(private val editAgeUseCase: EditAgeUseCase) : ViewModel()
{
    fun editAge(age: Int)
    {
        editAgeUseCase.execute(age)
    }
}