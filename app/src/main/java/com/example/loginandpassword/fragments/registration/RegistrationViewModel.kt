package com.example.loginandpassword.fragments.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.RegistrationUseCase
import com.example.domain.model.RegistrationUserModel

class RegistrationViewModel(private val registrationUseCase: RegistrationUseCase) : ViewModel()
{

    private val _registrationLiveData = MutableLiveData<Boolean>()
    val registrationLiveData: LiveData<Boolean> = _registrationLiveData

    fun registration(model: RegistrationUserModel)
    {

        val result = registrationUseCase.execute(param = model)
        _registrationLiveData.value = result
    }
}