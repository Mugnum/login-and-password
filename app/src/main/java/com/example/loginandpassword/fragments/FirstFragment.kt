package com.example.loginandpassword.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.loginandpassword.fragments.base.BaseFragment
import com.example.loginandpassword.databinding.FragmentFirstBinding

class FirstFragment : BaseFragment<FragmentFirstBinding>() {

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFirstBinding = FragmentFirstBinding.inflate(inflater, container, false)

    override fun FragmentFirstBinding.onBindView(saveInstanceState: Bundle?) {
        login.setOnClickListener {
            navController.navigate(FirstFragmentDirections.navigateToLoginFragment())
        }
        registration.setOnClickListener {
            navController.navigate(FirstFragmentDirections.navigateToRegistrationFragment())
        }
    }
}