package com.example.loginandpassword.fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.domain.model.UserCredo
import com.example.loginandpassword.fragments.base.BaseFragment
import com.example.loginandpassword.databinding.FragmentLoginBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<FragmentLoginBinding>()
{

    private val viewModel by viewModel<LoginViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater ,
        container: ViewGroup?
    ): FragmentLoginBinding = FragmentLoginBinding.inflate(inflater ,container ,false)

    override fun FragmentLoginBinding.onBindView(saveInstanceState: Bundle?)
    {
        submit.setOnClickListener {
            val userCredo = UserCredo(
                login = addName.text.toString() ,
                password = enterPassword.text.toString()
            )
            viewModel.login(userCredo)
            viewModel.loginLiveData.observe(viewLifecycleOwner) { result ->
                if (result)
                {
                    navController.navigate(
                        LoginFragmentDirections.navigateToMainFragment()
                    )
                } else
                {
                    Toast.makeText(requireContext() ,"Enter Credo" ,Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}