package com.example.loginandpassword.fragments.profile.email

import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.EditEmailUseCase

class ProfileEmailViewModel(private val editEmailUseCase: EditEmailUseCase) : ViewModel()
{
    fun editEmail(email: String)
    {
        editEmailUseCase.execute(email)
    }
}