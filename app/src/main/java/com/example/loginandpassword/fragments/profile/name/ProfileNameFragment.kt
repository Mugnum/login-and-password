package com.example.loginandpassword.fragments.profile.name

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.loginandpassword.databinding.FragmentProfileNameBinding
import com.example.loginandpassword.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileNameFragment : BaseFragment<FragmentProfileNameBinding>()
{

    private val viewModel by viewModel<ProfileNameViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater ,
        container: ViewGroup?
    ): FragmentProfileNameBinding = FragmentProfileNameBinding.inflate(inflater ,container ,false)

    override fun FragmentProfileNameBinding.onBindView(saveInstanceState: Bundle?)
    {
        submit.setOnClickListener {
            viewModel.editName(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}