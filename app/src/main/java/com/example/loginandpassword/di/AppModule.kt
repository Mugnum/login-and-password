package com.example.loginandpassword.di

import com.example.loginandpassword.fragments.creditCard.CreditCardViewModel
import com.example.loginandpassword.fragments.login.LoginViewModel
import com.example.loginandpassword.fragments.profile.ProfileViewModel
import com.example.loginandpassword.fragments.profile.age.ProfileAgeViewModel
import com.example.loginandpassword.fragments.profile.email.ProfileEmailViewModel
import com.example.loginandpassword.fragments.profile.name.ProfileNameViewModel
import com.example.loginandpassword.fragments.profile.number.ProfileNumberViewModel
import com.example.loginandpassword.fragments.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<RegistrationViewModel> { RegistrationViewModel(get()) }

    viewModel<ProfileViewModel> { ProfileViewModel(get()) }

    viewModel<ProfileNumberViewModel> { ProfileNumberViewModel(get()) }

    viewModel<ProfileNameViewModel> { ProfileNameViewModel(get()) }

    viewModel<ProfileAgeViewModel> { ProfileAgeViewModel(get()) }

    viewModel<ProfileEmailViewModel> { ProfileEmailViewModel(get()) }

    viewModel<LoginViewModel> { LoginViewModel(get()) }

    viewModel<CreditCardViewModel>{CreditCardViewModel(get())}
}

