package com.example.loginandpassword.di

import com.example.domain.UseCase.*
import org.koin.dsl.module

val domainModule = module {

    factory<EditAgeUseCase> { EditAgeUseCase(get()) }

    factory<EditEmailUseCase> { EditEmailUseCase(get()) }

    factory<EditNameUseCase> { EditNameUseCase(get()) }

    factory<EditNumberUseCase> { EditNumberUseCase(get()) }

    factory<GetUserInformationUseCase> { GetUserInformationUseCase(get()) }

    factory<LoginUseCase> { LoginUseCase(get()) }

    factory<RegistrationUseCase> { RegistrationUseCase(get()) }

    factory<GetCreditCardUseCase> { GetCreditCardUseCase(get()) }
}