package com.example.domain.repository

import com.example.domain.model.UserProfileInfo

interface UserProfileRepository
{
    fun getUserProfileInfo(): UserProfileInfo

    fun editProfileNumber(number: String)

    fun editProfileName(name: String)

    fun editProfileAge(age: Int)

    fun editProfileEmail(email: String)
}