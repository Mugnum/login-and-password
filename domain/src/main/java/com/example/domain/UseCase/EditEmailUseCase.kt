package com.example.domain.UseCase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserProfileRepository

class EditEmailUseCase(private val userProfileRepository: UserProfileRepository) :
    UseCase<String ,Unit>
{
    override fun execute(param: String?)
    {
        userProfileRepository.editProfileEmail(param!!)
    }
}