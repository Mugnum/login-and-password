package com.example.domain.UseCase

import com.example.domain.base.UseCase
import com.example.domain.model.UserProfileInfo
import com.example.domain.repository.UserProfileRepository

class GetUserInformationUseCase(private val userProfileRepository: UserProfileRepository) :
    UseCase<Unit ,UserProfileInfo>
{
    override fun execute(param: Unit?): UserProfileInfo
    {
        return userProfileRepository.getUserProfileInfo()
    }
}