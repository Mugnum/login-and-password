package com.example.domain.UseCase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserProfileRepository

class EditAgeUseCase(private val userProfileRepository: UserProfileRepository) : UseCase<Int ,Unit>
{
    override fun execute(param: Int?)
    {
        userProfileRepository.editProfileAge(param !!)
    }
}