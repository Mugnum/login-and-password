package com.example.domain.UseCase

import com.example.domain.base.UseCase
import com.example.domain.model.UserCredo
import com.example.domain.repository.AuthRepository

class LoginUseCase(private val authRepository: AuthRepository) : UseCase<UserCredo, Boolean> {
    override fun execute(param: UserCredo?): Boolean {
        return authRepository.checkUserCredo(param!!)
    }
}