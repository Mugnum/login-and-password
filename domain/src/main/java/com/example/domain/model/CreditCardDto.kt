package com.example.domain.model

data class CreditCardDto(
    val type: String,
    val number: String,
    val expiration: String,
    val owner: String
) {
}