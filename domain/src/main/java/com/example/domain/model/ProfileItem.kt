package com.example.domain.model

data class ProfileItem(
    val title: String ,
    val subtitle: String ,
    val action: () -> Unit
)