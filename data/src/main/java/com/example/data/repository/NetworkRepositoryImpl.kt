package com.example.data.repository

import com.example.data.converters.CreditCardDtoConverter
import com.example.data.storage.network.NetworkStorage
import com.example.domain.model.CreditCardDto
import com.example.domain.repository.NetworkRepository

class NetworkRepositoryImpl(private val networkStorage: NetworkStorage, private val creditCardDtoConverter: CreditCardDtoConverter) : NetworkRepository {
    override suspend fun getCreditCard(count: Int): List<CreditCardDto> {
        try {
            val response = networkStorage.getCreditCard(count)
            if (response.isSuccessful) {
                response.body()?.let { baseResponse ->
                    val creditCard = mutableListOf<CreditCardDto>()
                    baseResponse.data?.map {
                        creditCard.add(creditCardDtoConverter.invoke(it))
                    }
                    return creditCard
                }
            }
        }
        catch (e: Exception) {
            println(e.message)
        }
        return emptyList()
    }
}