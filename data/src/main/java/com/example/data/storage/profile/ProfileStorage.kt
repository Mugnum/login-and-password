package com.example.data.storage.profile

import com.example.domain.model.UserProfileInfo

interface ProfileStorage
{
    fun getProfileInfo():UserProfileInfo

    fun editPhoneNumber(number: String)

    fun editUserName(name: String)

    fun editUserAge(age: Int)

    fun editUserEmail(email: String)
}