package com.example.data.storage.profile

import android.content.Context
import androidx.core.content.edit
import com.example.domain.model.UserProfileInfo

class ProfileStoragePrefImpl(context: Context) : ProfileStorage
{

    private val sharedPref = context.getSharedPreferences(PROFILE_PREF_NAME ,Context.MODE_PRIVATE)

    override fun getProfileInfo(): UserProfileInfo
    {
        return UserProfileInfo(
            number = sharedPref.getString(NUMBER_KEY ,DEFAULT_NUMBER) ?: EMPTY_STRING ,
            name = sharedPref.getString(NAME_KEY ,DEFAULT_NAME) ?: EMPTY_STRING ,
            age = sharedPref.getInt(AGE_KEY ,DEFAULT_AGE) ,
            email = sharedPref.getString(EMAIL_KEY ,DEFAULT_EMAIL) ?: EMPTY_STRING
        )
    }

    override fun editPhoneNumber(number: String)
    {
        sharedPref.edit {
            putString(NUMBER_KEY, number)
        }
    }

    override fun editUserName(name: String)
    {
       sharedPref.edit {
           putString(NAME_KEY, name)
       }
    }

    override fun editUserAge(age: Int)
    {
        sharedPref.edit {
            putInt(AGE_KEY, age)
        }
    }

    override fun editUserEmail(email: String)
    {
        sharedPref.edit {
            putString(EMAIL_KEY, email)
        }
    }

    companion object
    {

        const val PROFILE_PREF_NAME = "PROFILE_PREF_NAME"
        const val NUMBER_KEY = "NUMBER_KEY"
        const val NAME_KEY = "NAME_KEY"
        const val AGE_KEY = "AGE_KEY"
        const val EMAIL_KEY = "EMAIL_KEY"

        const val DEFAULT_NAME = "profile name"
        const val DEFAULT_NUMBER = "+375555555555"
        const val DEFAULT_EMAIL = "email@gmail.com"
        const val DEFAULT_AGE = 26

        const val EMPTY_STRING = ""
    }
}