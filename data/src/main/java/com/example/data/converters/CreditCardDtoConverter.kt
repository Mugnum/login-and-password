package com.example.data.converters

import com.example.data.storage.network.model.CreditCard
import com.example.domain.base.Converter
import com.example.domain.model.CreditCardDto

class CreditCardDtoConverter : Converter<CreditCard, CreditCardDto> {
    override fun invoke(params: CreditCard): CreditCardDto {
        return CreditCardDto(
            params.type ?: "",
            params.number ?: "",
            params.expiration ?: "",
            params.owner ?: ""
        )
    }
}